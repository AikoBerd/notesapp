package com.example.notesapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapp.R
import com.example.notesapp.entity.Note
import java.text.SimpleDateFormat
import java.util.*

class NoteListAdapter(private val onItemClick: ((Note) -> Unit)?): RecyclerView.Adapter<NoteListAdapter.ViewHolder>() {

    private val noteList = mutableListOf<Note>()

    fun submitList(newList: List<Note>) {
        noteList.clear()
        noteList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(noteList[position])
    }

    override fun getItemCount(): Int = noteList.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val nameTextView: TextView = view.findViewById(R.id.nameTextView)
        private val dateTextView: TextView = view.findViewById(R.id.dateTextView)

        fun onBind(note: Note){
            nameTextView.text = note.name
            dateTextView.text = formatDate(note.date)
            itemView.setOnClickListener {
                onItemClick?.invoke(note)
            }
        }
    }

    private fun formatDate(date: Date): String {
        val dateFormat = SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.getDefault())
        return dateFormat.format(date)
    }
}

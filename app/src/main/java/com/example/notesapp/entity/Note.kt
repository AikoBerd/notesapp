package com.example.notesapp.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.notesapp.data.DateConverter
import java.util.*

@Entity(tableName = "notes")
@TypeConverters(DateConverter::class)
data class Note(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    val name: String,
    val date: Date,
    val noteText: String
)
package com.example.notesapp.data

import androidx.room.TypeConverter
import java.util.*

class DateConverter {
    @TypeConverter
    fun toTimestamp(value: Long): Date {
        return Date(value)
    }

    @TypeConverter
    fun toDate(timestamp: Date): Long {
        return timestamp.time
    }
}
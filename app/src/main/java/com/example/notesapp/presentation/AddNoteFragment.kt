package com.example.notesapp.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.notesapp.R
import com.example.notesapp.data.NoteDatabase
import com.example.notesapp.data.NoteRepository
import com.example.notesapp.entity.Note
import java.util.*

class AddNoteFragment: Fragment() {

    private val noteDatabase by lazy { NoteDatabase.getDatabase(requireContext()).noteDao() }
    private val repository by lazy { NoteRepository(noteDatabase) }

    private val viewModel: NoteViewModel by viewModels {
        NoteViewModelFactory(repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add_note, container, false)

        val noteId = arguments?.getLong("NOTE_ID_KEY", 0L) ?: 0L
        val descriptionKey = arguments?.getString("DESCRIPTION_KEY", "") ?: ""
        val nameKey = arguments?.getString("NAME_KEY", "") ?: ""

        view.findViewById<Button>(R.id.saveBtn).setOnClickListener {
            val nameEditText = view.findViewById<EditText>(R.id.nameEditText)
            val name =nameEditText.text.toString()
            val descriptionEditText = view.findViewById<EditText>(R.id.descriptionEditText)
            val description = descriptionEditText.text.toString()
            val date = Date()

            val note = Note(name = name, date = date, noteText = description)
            if (noteId == 0L) {
                viewModel.insertNote(note)
            } else {
                note.id = noteId
                nameEditText.setText(nameKey)
                descriptionEditText.setText(descriptionKey)
                nameEditText.isEnabled = false
                viewModel.updateNote(note)
            }
            findNavController().navigateUp()
        }
        return view
    }
}
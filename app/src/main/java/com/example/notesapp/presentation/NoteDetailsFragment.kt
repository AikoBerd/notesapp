package com.example.notesapp.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.notesapp.R
import com.example.notesapp.data.NoteDatabase
import com.example.notesapp.data.NoteRepository

class NoteDetailsFragment : Fragment() {

    private val noteDatabase by lazy { NoteDatabase.getDatabase(requireContext()).noteDao() }
    private val repository by lazy { NoteRepository(noteDatabase) }
    private val viewModel: NoteViewModel by viewModels {
        NoteViewModelFactory(repository)
    }
    private lateinit var name: TextView
    private lateinit var description: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_note_details, container, false)

        val noteId = arguments?.getLong("NOTE_ID_KEY", 0L) ?: 0L

        name = view.findViewById(R.id.nameDetailTextView)
        description = view.findViewById(R.id.descriptionTextView)

        viewModel.getNoteById(noteId).observe(viewLifecycleOwner) { note ->
            note?.let {
                name.text = it.name
                description.text = it.noteText
            }
        }

        view.findViewById<Button>(R.id.editNoteBtn).setOnClickListener {
            val nameText = name.text.toString()
            val descriptionText = description.text.toString()

            val bundle = Bundle().apply {
                putLong("NOTE_ID_KEY", noteId)
                putString("NAME_KEY", nameText)
                putString("DESCRIPTION_KEY", descriptionText)
            }

            val navController = Navigation.findNavController(view)
            navController.navigate(R.id.action_noteDetailsFragment_to_addNoteFragment, bundle)
        }

        view.findViewById<Button>(R.id.deleteNoteBtn).setOnClickListener {
            viewModel.deleteNote(noteId)
            findNavController().navigateUp()
        }

        return view
    }
}
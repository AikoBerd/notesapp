package com.example.notesapp.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapp.R
import com.example.notesapp.adapter.NoteListAdapter
import com.example.notesapp.data.NoteDatabase
import com.example.notesapp.data.NoteRepository

class NoteListFragment: Fragment() {

    private lateinit var adapter: NoteListAdapter
    private val noteDatabase by lazy { NoteDatabase.getDatabase(requireContext()).noteDao() }
    private val repository by lazy { NoteRepository(noteDatabase) }

    private val viewModel: NoteViewModel by viewModels {
        NoteViewModelFactory(repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_note_list, container, false)

        adapter = NoteListAdapter { note ->
            val bundle = Bundle().apply {
                putLong("NOTE_ID_KEY", note.id)
            }
            val navController = Navigation.findNavController(view)
            navController.navigate(R.id.action_noteListFragment_to_noteDetailsFragment, bundle)
        }

        val noteRV = view.findViewById<RecyclerView>(R.id.noteRV)
        val layoutManager = LinearLayoutManager(requireContext())
        noteRV.layoutManager = layoutManager
        noteRV.adapter = adapter

        view.findViewById<Button>(R.id.createNoteBtn).setOnClickListener {
            val navController = Navigation.findNavController(view)
            navController.navigate(R.id.action_noteListFragment_to_addNoteFragment)
        }

        viewModel.allNotes.observe(viewLifecycleOwner) { notes ->
            adapter.submitList(notes)
        }

        return view
    }
}